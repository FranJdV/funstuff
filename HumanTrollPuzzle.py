import numpy as np

BEGIN = np.array([[3, 3], [0, 0]])
END = np.array([[0, 0], [3, 3]])

MOVE = {"go": [np.array([[-1, -1], [1, 1]]),
               np.array([[-2, 0], [2, 0]]),
               np.array([[0, -2], [0, 2]]),
               np.array([[-1, 0], [1, 0]]),
               np.array([[0, -1], [0, 1]])],
        "back": [np.array([[1, 1], [-1, -1]]),
                 np.array([[2, 0], [-2, 0]]),
                 np.array([[0, 2], [0, -2]]),
                 np.array([[1, 0], [-1, 0]]),
                 np.array([[0, 1], [0, -1]])]}


def check_possible(cur_state) -> bool:
    if (cur_state < 0).sum() > 0 or (cur_state >= 4).sum() > 0:
        return False
    if cur_state[0, 0] != 0 and cur_state[0, 1] > cur_state[0, 0]:
        return False
    if cur_state[1, 0] != 0 and cur_state[1, 1] > cur_state[1, 0]:
        return False
    return True


def run(cur_states: list, direction: str) -> list:
    next_states = list()
    for e in cur_states:
        for m in MOVE[direction]:
            new_state = e[len(e)-1][2]+m
            if check_possible(new_state):
                t = [(i[0], i[1], i[2]) for i in e]
                t.append((m, direction, new_state))
                next_states.append(t)

    if direction == "go":
        for n in next_states:
            if (n[len(n)-1][2] == END).all():
                return n
    return run(next_states, "back" if direction == "go" else "go")


if __name__ == '__main__':
    init = [[(None, "BEGIN", BEGIN)]]
    result = run(init, "go")
    for r in result:
        print(r)
